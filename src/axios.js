import axios from 'axios';

const instance = axios.create({
  // the API (the cloud function) URL
  baseURL: 'https://us-central1-clone-eeb88.cloudfunctions.net/api',
});

export default instance;
