import firebase from 'firebase';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyAQWxJEIB-wfJn0AJeecygOtSxHe9yf7kk',
  authDomain: 'clone-eeb88.firebaseapp.com',
  databaseURL: 'https://clone-eeb88.firebaseio.com',
  projectId: 'clone-eeb88',
  storageBucket: 'clone-eeb88.appspot.com',
  messagingSenderId: '885302702525',
  appId: '1:885302702525:web:36b2fd23edb6735d23e2d4',
  measurementId: 'G-QRNRYFP3WF',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebaseApp.auth();

export {db, auth};
