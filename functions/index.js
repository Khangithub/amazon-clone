const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const stripe = require('stripe')(
  'sk_test_51Hm2DHBfJzCvHYxxU0MLHDUwLxkveAgpFpyZmwxCI1cZGLu6gn9qcOJBcomI2ZXvuRXBW4Vf5nKsx1iReMCJ9PoQ00sR8rwGDR'
); // Secret API key

// API

// - API config
const app = express();

// - Middlewares
app.use(cors({origin: true}));
app.use(express.json());

// - API routes
app.get('/', (request, response) => response.status(200).send('hello world'));

app.post('/payments/create', async (request, response) => {
  const total = request.query.total;

  console.log('Payment Request Received for this amount >>>>>>', total);

  const paymentIntent = await stripe.paymentIntents.create({
    amount: total, // submits of the currency
    currency: 'usd',
  });

  // ok - created
  response.status(201).send({
    clientSecret: paymentIntent.client_secret,
  });
});

// - Listen command
exports.api = functions.https.onRequest(app);

// Example Enpoit
// http://localhost:5001/clone-eeb88/us-central1/api
